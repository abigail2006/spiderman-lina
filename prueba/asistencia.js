// Generar calendario sincronizado con la fecha actual
const calendarContainer = document.getElementById("calendar");
const attendanceData = {}; // Objeto para almacenar asistencia por día
const currentDate = new Date();
const currentMonth = currentDate.getMonth(); // Mes actual (0-11)
const currentYear = currentDate.getFullYear(); // Año actual
const currentDay = currentDate.getDate(); // Día actual

// Función para obtener el número de días en el mes
function getDaysInMonth(month, year) {
    return new Date(year, month + 1, 0).getDate();
}

// Función para obtener el primer día del mes (0 = Domingo, 1 = Lunes, etc.)
function getFirstDayOfMonth(month, year) {
    return new Date(year, month, 1).getDay();
}

// Generar los días del mes actual
function generateCalendar() {
    calendarContainer.innerHTML = ''; // Limpiar el calendario
    const daysInMonth = getDaysInMonth(currentMonth, currentYear); // Obtener días del mes actual
    const firstDay = getFirstDayOfMonth(currentMonth, currentYear); // Primer día del mes

    // Añadir días vacíos si el primer día del mes no es lunes (para alinear con el día correcto)
    for (let i = 0; i < firstDay; i++) {
        const emptyDiv = document.createElement("div");
        calendarContainer.appendChild(emptyDiv); // Agregar espacios vacíos al principio
    }

    // Añadir los días del mes
    for (let i = 1; i <= daysInMonth; i++) {
        const dayDiv = document.createElement("div");
        dayDiv.className = "day";
        dayDiv.textContent = i;

        if (i === currentDay) {
            dayDiv.classList.add("today"); // Resaltar el día actual
        }

        // Agregar evento al hacer clic
        dayDiv.addEventListener("click", () => selectDate(i));
        calendarContainer.appendChild(dayDiv);
    }
}

generateCalendar();

// Función para seleccionar una fecha
function selectDate(day) {
    const status = attendanceData[day];
    updateCalendar();
    if (status === "present") {
        alert(`Día ${day}: Presente`);
    } else if (status === "absent") {
        alert(`Día ${day}: Ausente`);
    } else {
        const confirmPresente = confirm(`Marcar asistencia para el día ${day} como "Presente"?`);
        attendanceData[day] = confirmPresente ? "present" : "absent";
    }
    updateCalendar();
    updateChart();
}

// Actualizar el calendario con los colores de asistencia
function updateCalendar() {
    document.querySelectorAll(".day").forEach(dayDiv => {
        const day = parseInt(dayDiv.textContent);
        dayDiv.classList.remove("present", "absent");
        if (attendanceData[day] === "present") {
            dayDiv.classList.add("present");
        } else if (attendanceData[day] === "absent") {
            dayDiv.classList.add("absent");
        }
    });
}

// Función para actualizar el gráfico de dona
function updateChart() {
    const canvas = document.getElementById("attendanceChart");
    const ctx = canvas.getContext("2d");

    const totalDays = Object.keys(attendanceData).length;
    const presentDays = Object.values(attendanceData).filter(status => status === "present").length;
    const absentDays = totalDays - presentDays;

    const presentPercentage = (presentDays / totalDays) * 100 || 0;
    const absentPercentage = (absentDays / totalDays) * 100 || 0;

    // Limpia el canvas
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    // Configuración del gráfico
    const centerX = canvas.width / 2;
    const centerY = canvas.height / 2;
    const radius = 90;
    const innerRadius = radius / 2;

    // Ángulo inicial
    let startAngle = -0.5 * Math.PI;

    // Dibuja la parte de "Presente" (verde)
    const presentEndAngle = startAngle + (presentPercentage / 100) * 2 * Math.PI;
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, startAngle, presentEndAngle);
    ctx.lineTo(centerX, centerY);
    ctx.fillStyle = "#25c24c";
    ctx.fill();

    // Actualiza el ángulo inicial para la siguiente sección
    startAngle = presentEndAngle;

    // Dibuja la parte de "Ausente" (rojo)
    const absentEndAngle = startAngle + (absentPercentage / 100) * 2 * Math.PI;
    ctx.beginPath();
    ctx.arc(centerX, centerY, radius, startAngle, absentEndAngle);
    ctx.lineTo(centerX, centerY);
    ctx.fillStyle = "#c52b23";
    ctx.fill();

    // Dibuja el agujero de la dona
    ctx.beginPath();
    ctx.arc(centerX, centerY, innerRadius, 0, 2 * Math.PI);
    ctx.fillStyle = "#ffffff";
    ctx.fill();

    // Muestra el porcentaje en texto
    document.getElementById("percentageText").textContent = `Presente: ${presentPercentage.toFixed(1)}%, Ausente: ${absentPercentage.toFixed(1)}%`;
}